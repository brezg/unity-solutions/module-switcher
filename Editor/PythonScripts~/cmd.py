import subprocess
from pathlib import Path
from typing import List


def run_subprocess(args: List[str], cwd: Path) -> str:
    process = subprocess.run(args,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE,
                             universal_newlines=True,
                             cwd=cwd)

    if process.returncode != 0:
        raise Exception(f'{args} on {cwd} returned {process.returncode}\n--\n{process.stdout}\n--\n{process.stderr}')

    return process.stdout


def run_command(args: List[str], path: Path) -> None:
    command = ''

    for arg in args:
        command += f'{arg} '

    print(f'> {path}: {command}')
    run_subprocess(args, path)

