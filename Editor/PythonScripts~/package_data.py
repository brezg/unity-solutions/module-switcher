from pathlib import Path


class PackageData:
    project_path: Path
    submodule_name: str
    submodule_path: Path
    submodule_local_path: Path
    git_url: str
    version: str
    manifest_path: Path
    gitmodules_path: Path
    gitconfig_path: Path

    def __init__(self, project_path: Path, submodule_name: str, submodule_path: Path, git_url: str, version: str):
        self.project_path = project_path
        self.submodule_name = submodule_name
        self.submodule_local_path = submodule_path
        self.submodule_path = self.project_path / submodule_path
        self.git_url = git_url
        self.version = version
        self.manifest_path = self.project_path / "Packages" / "manifest.json"
        self.gitmodules_path = self.project_path / '.gitmodules'
        self.gitconfig_path = self.project_path / '.git' / 'config'
