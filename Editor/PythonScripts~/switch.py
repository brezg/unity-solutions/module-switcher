import json
import re
import subprocess
import sys
from pathlib import Path
from typing import Tuple

from cmd import run_command
from package_data import PackageData


def main() -> None:
    print("Module switch initiated")
    project_path = Path(sys.argv[1]).parent
    submodule_name = sys.argv[2]
    submodule_path = Path(sys.argv[3].strip('/'))
    git_url = sys.argv[4]
    version = sys.argv[5]
    is_submodule_needed = sys.argv[6] == "1"

    package = PackageData(project_path, submodule_name, submodule_path, git_url, version)

    is_submodule_active = check_is_submodule_active(package)

    if is_submodule_active and not is_submodule_needed:
        switch_to_package(package)
    elif not is_submodule_active and is_submodule_needed:
        switch_to_submodule(package)
    else:
        print("Already in needed mode")

    # subprocess.run(['git', 'status'])


def check_is_submodule_active(package: PackageData) -> bool:
    print("Checking current state...")
    if not package.gitmodules_path.exists():
        print("  Current state is: package")
        return False

    regex = re.compile(r"^submodule\.(.+)\.path$")

    output = get_submodules_names(package.project_path)
    modules = output.split('\n')

    for (index, value) in enumerate(modules):
        search = re.search(regex, value)

        if search is None:
            continue

        module = search.group(1)

        if module == package.submodule_name:
            print("  Current state is: submodule")
            return True

    print("  Current state is: package")
    return False


def switch_to_package(package: PackageData) -> None:
    print("Switching to package...")
    remove_submodule(package)
    set_dependency_to_git(package)

    print("Switched to package")


def remove_submodule(package: PackageData) -> None:
    gitmodules_pattern = re.compile(rf'\[submodule\s\"{package.submodule_name}\"]\n\tpath = (.+)\n\turl = (.+)\n')
    git_config_pattern = re.compile(rf'\[submodule\s\"{package.submodule_name}\"]\n\turl = (.+)\n(\tactive = .+\n)?')

    submodule_path, git_url = replace_file_content_with_pattern_and_get_groups(package.gitmodules_path, gitmodules_pattern)

    run_command(['git', 'add', '.gitmodules'], package.project_path)

    replace_file_content_with_pattern_and_get_groups(package.gitconfig_path, git_config_pattern)

    run_command(['rm', '-rf', f'.git/modules/{submodule_path}'], package.project_path)

    run_command(['rm', '-rf', submodule_path], package.project_path)

    if len(package.gitmodules_path.read_text()) == 0:
        run_command(['rm', '-rf', package.gitmodules_path], package.project_path)

    run_command(['git', 'reset'], package.project_path)


def set_dependency_to_git(package: PackageData) -> None:
    content = json.load(package.manifest_path.open())

    for (index, package_id) in enumerate(content["dependencies"]):
        link: str = content["dependencies"][package_id]
        if str(package.submodule_path) in link:
            content["dependencies"][package_id] = f'{package.git_url}#{package.version}'

    json.dump(content, package.manifest_path.open('w'), ensure_ascii=False, indent=2)


def add_submodule(package: PackageData) -> None:
    run_command(['git', 'submodule', 'add', '--name', package.submodule_name, '--force', package.git_url, package.submodule_local_path], package.project_path)
    run_command(['git', 'fetch'], package.submodule_path)
    run_command(['git', 'checkout', f'tags/{package.version}'], package.submodule_path)
    run_command(['git', 'reset'], package.project_path)


def switch_to_submodule(package: PackageData) -> None:
    print("Switching to submodule...")
    add_submodule(package)
    set_dependency_to_submodule(package)
    print("Switched to submodule")


def set_dependency_to_submodule(package: PackageData) -> None:
    content = json.load(package.manifest_path.open())

    for (index, package_id) in enumerate(content["dependencies"]):
        link: str = content["dependencies"][package_id]
        if str(package.git_url) in link:
            content["dependencies"][package_id] = f'file:{package.submodule_path}'

    json.dump(content, package.manifest_path.open('w'), ensure_ascii=False, indent=2)


def get_submodules_names(cwd: Path) -> str:
    process = subprocess.run(['git', 'config', '--file', '.gitmodules', '--name-only', '--get-regexp', 'path'],
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE,
                             universal_newlines=True,
                             cwd=cwd)
    
    return process.stdout


def replace_file_content_with_pattern_and_get_groups(file_path: Path, pattern: re.Pattern) -> Tuple[str, ...]:
    with open(file_path, 'r') as file:
        file_data = file.read()

    groups = re.search(pattern, file_data).groups()

    file_data = re.sub(pattern, "", file_data)

    with open(file_path, 'w') as file:
        file.write(file_data)

    return groups


if __name__ == '__main__':
    main()
