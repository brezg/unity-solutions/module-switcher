using System.Collections.Generic;
using UnityEngine;


namespace Brezg.Utils.Editor.ModuleSwitcher
{
	[CreateAssetMenu(fileName = "ModulesConfig", menuName = "Brezg/Сабмодули-пакеты")]
	public class ModulesConfig : ScriptableObject
	{
		[SerializeField]
		private List<ModuleData> _modules;


		public void SetupUpdate()
		{
			foreach (var moduleData in _modules)
				moduleData.SetContext(this);
		}
	}
}
