using System.Diagnostics;
using System.Linq;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities;
using Sirenix.Utilities.Editor;
using UnityEditor;
using UnityEngine;
using Debug = UnityEngine.Debug;


namespace Brezg.Utils.Editor.ModuleSwitcher
{
	public class ModuleSwitchWindow : OdinEditorWindow
	{
		private const string _SelfPackageName = "com.brezg.module-switcher";
		
		[SerializeField]
		[InlineEditor(InlineEditorObjectFieldModes.Boxed, Expanded = true)]
		private ModulesConfig _modules;


		[MenuItem("Tools/Modules Mode Switcher")]
		private static void OpenWindow()
		{
			var window = GetWindow<ModuleSwitchWindow>();

			window.position = GUIHelper.GetEditorWindowRect().AlignCenter(700, 700);
			window.Init();
		}


		private void Init()
		{
			var assetIds = AssetDatabase.FindAssets($"t:{nameof(ModulesConfig)}");

			_modules = assetIds
				.Select(id => AssetDatabase.LoadAssetAtPath<ModulesConfig>(
						AssetDatabase.GUIDToAssetPath(id)
					)
				)
				.First();
			
			_modules.SetupUpdate();
		}


		public static void Switch(ModuleData module)
		{
			Debug.Log($"Switching {module.SubmoduleName} to {module.Mode}");
			var pythonScriptPath = System.IO.Path.GetFullPath($"Packages/{_SelfPackageName}/Editor/PythonScripts~/switch.py");
			var projectPath = $"{Application.dataPath}";

			var psi = new ProcessStartInfo();
			psi.FileName = "python3";
			psi.UseShellExecute = false;
			psi.RedirectStandardOutput = true;
			psi.RedirectStandardError = true;
			psi.Arguments = $"{pythonScriptPath} {projectPath} {module.Args}";
			
			Debug.Log($"Running with args:\n {projectPath} {module.Args}");

			var p = Process.Start(psi);
			var strOutput = p.StandardOutput.ReadToEnd();
			var errOutput = p.StandardError.ReadToEnd();
			p.WaitForExit();
			
			Debug.Log(strOutput);

			if (!string.IsNullOrEmpty(errOutput))
				Debug.LogError(errOutput);
			
			EditorUtility.RequestScriptReload();
		}
	}
}
