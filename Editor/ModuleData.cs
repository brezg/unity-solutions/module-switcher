using System;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;


namespace Brezg.Utils.Editor.ModuleSwitcher
{
	[Serializable]
	public class ModuleData
	{
		[SerializeField]
		[BoxGroup("Сабмодуль")]
		public string SubmoduleName;


		[SerializeField]
		[BoxGroup("Сабмодуль")]
		public string SubmodulePath;


		[SerializeField]
		[LabelWidth(50)]
		[BoxGroup("Репозиторий")]
		public string GitUrl;


		[SerializeField]
		[LabelWidth(50)]
		[BoxGroup("Репозиторий")]
		private Vector3Int _version;


		[SerializeField]
		[EnumToggleButtons, LabelWidth(50)]
		[OnValueChanged(nameof(Switch))]
		[PropertySpace(SpaceAfter = 40)]
		public ModuleMode Mode = ModuleMode.Package;


		private ModulesConfig _contextOwner;


		public string Args
			=> $"{SubmoduleName} {SubmodulePath} {GitUrl} {Version} {IsSubmoduleNeededArg}";


		private int IsSubmoduleNeededArg
			=> Mode == ModuleMode.Submodule ? 1 : 0;


		public string Version
			=> $"v{_version.x}.{_version.y}.{_version.z}";


		private void Switch(ModuleMode mode)
		{
			Mode = mode;
			
			if (_contextOwner != null)
			{
				EditorUtility.SetDirty(_contextOwner);
				AssetDatabase.SaveAssetIfDirty(_contextOwner);
			}
			
			ModuleSwitchWindow.Switch(this);
		}


		public void SetContext(ModulesConfig context)
			=> _contextOwner = context;
	}
}
